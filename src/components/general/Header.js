import React from 'react'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'
import { Icon } from '@ant-design/react-native';
import {
    HeaderContainer,
    HeaderText2,
    HeaderIcon,
    Center,
} from '../General.styled'
import { TouchableOpacity } from 'react-native'
class Header extends React.Component {
    render() {
        const { title = '', hideGoBack, goBack } = this.props
        return (
            <HeaderContainer>
                {hideGoBack ? null : (
                    <HeaderIcon onPress={goBack}>
                        <Icon name="close" color="#0074D9" />
                    </HeaderIcon>
                )}
                <Center style={{ flex: 1 }}>
                    <HeaderText2>{title}</HeaderText2>
                </Center>
            </HeaderContainer>
        )
    }

}
export default connect(
    null,
    { goBack }
)(Header)
