import React from 'react'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { TouchableOpacity } from 'react-native'
import { Icon } from '@ant-design/react-native'
import {
    HeaderContainer,
    HeaderText2,
    HeaderIcon,
    Body,
    Center
} from '../General.styled'
import { ROUTE_TO_MAIN,ROUTE_TO_ADDWALLET } from "../../constants/routeConstants";

class Footer extends React.Component {
    render() {
        const { goToHome, goToAddWallet} = this.props
        return (
            <HeaderContainer >
                <HeaderIcon onPress={goToHome}>
                    <Icon name="home" color="#0074D9" />
                </HeaderIcon>
                <Body>
                    <Center>
                        <TouchableOpacity onPress={goToAddWallet} >
                            <HeaderText2>ADD Wallet</HeaderText2>
                        </TouchableOpacity>
                    </Center>
                </Body>
                <HeaderIcon >
                    <Icon name="user" color="#0074D9" />
                </HeaderIcon>
            </HeaderContainer>
        )
    }
}

export default connect(
    null,
    dispatch => ({
        goToHome: state => dispatch(push(ROUTE_TO_MAIN)),
        goToAddWallet: state => dispatch(push(ROUTE_TO_ADDWALLET)),
        // goToProfile: state => dispatch(push('/profile')),
    })
)(Footer)