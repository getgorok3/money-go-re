import React from 'react'
import { Body, Content, ThemeBackground } from '../General.styled'
import Footer from './Footer'
import Header from './Header'
import { images } from "../../utilities";

class Container extends React.Component {
    render() {
        const { children, title, hideGoBack, hideFooter } = this.props
        return (
            <Body>
                <Header title={title} hideGoBack={hideGoBack} />
                <ThemeBackground style={{ flex: 1 }} source={images.loginBackground}>
                    <Content>
                        {children}
                    </Content>
                </ThemeBackground>
                {hideFooter ? null : <Footer />}
            </Body>
        )
    }
}

export default Container
