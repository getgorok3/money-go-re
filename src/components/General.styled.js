import styled from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native';


export const Body = styled.View`
flex: 1;
flex-direction: column
`

export const Content = styled.View`
    padding-top: 8;
    padding-left: 16;
    padding-right: 16;
`

// center 
export const Center = styled.View`
    align-items: center;
    justify-content: center;
`
export const RowCenter = styled(Center)`
    flex-direction: row;
    background-color: white;
`
export const ColumnCenter = styled(Center)`
    flex-direction: column;
`
export const ViewColumn = styled.View`
    flex-direction: column;
    background-color: white;
`
export const ViewRow = styled.View`
    flex-direction: row;
    background-color: white;
`
export const AntFormContainer = styled.View`
    margin-right: 10%;
`
export const FlexContainer = styled.View`
    flex: 1;
`

// Tool bar f6f5f3
export const HeaderContainer = styled.View`
    padding-top: 8;
    padding-bottom: 8;
    background-color: #f6f5f3;
    flex-direction: row;
    align-items: center;
`
export const HeaderIcon = styled.TouchableOpacity`
    
    padding-left: 8;
    padding-right: 8;
`
export const HeaderText = styled.Text`
    color: white;
    font-size: 24;
`
export const HeaderText2 = styled.Text`
    color: #0074D9;
    font-size: 24;
`

//About image
// make everythings inside Body(flex,column) center
export const CenterContainer = styled(Body)`      
    justify-content: center;
    align-items: center;
`
// logo size
export const Logo = styled.Image`
    width: 170;
    height: 170;
    border-radius: 150;
`
export const ImageButton = styled.Image`
    width: 100;
    height: 50;
    border-radius: 20;
`
export const ThemeBackground = styled.ImageBackground`
    width: 100%;
    height: 100%;
`
//User input - Form 
export const NewInput = styled.TextInput`
    padding-top: 8;
    padding-bottom: 8;
    padding-left: 8;
    padding-right: 8;
    margin-bottom: 8;
    width: 100%;
    border-radius: 8;
    border-width: 1;
    border-style: solid;
    border-color: #540d6e;
`

// ant design 
export const UserInput = styled(InputItem)`
    width: 150;
    color: #0074D9;
`
export const SubmitButton = styled(Button)`
   width: 40%
`

// colors 
export const ButtonText = styled.Text`
   color: white;
`
export const LinkText = styled.Text`
   color: #AD66D5;
`
export const FlextContent = styled(Content)`
    padding-top: 10%;
    background-color: white;
    flex: 1;
`


export const SummaryText = styled.Text`
    font-size: 18;
    text-align: center;
`

export const MiniBox = styled.View`
    flex-direction: row;
`
export const MiniBoxXS = styled.View`
    width: 50%;
    flex-direction: column;
`



