import {
    USER_SAVE,
    WALLET_ADD
} from '../constants/appConstants'

// User
export const userSave = ({ email, password, firstname, lastname,imageUrl,id,bankBalance,token }) => ({
    type: USER_SAVE,
    payload: { email, password, firstname, lastname,imageUrl,id, bankBalance,token},
})

export const walletSave = ({ name, money, income, expense,transactions,id }) => ({
    type: WALLET_ADD,
    payload: { name, money, income, expense,transactions,id },
})