import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Image, FlatList, Text } from 'react-native';
import Container from '../components/general/Container';
import { WhiteSpace, Card, Icon } from '@ant-design/react-native';
import {
    Center, HeaderText, MiniBox, MiniBoxXS, CenterContainer,
    Logo, SummaryText, SubmitButton, FlextContent, ColumnCenter
} from '../components/General.styled'
import { images } from "../utilities";
var _ = require('lodash');
class WalletDetail extends Component {
    state = {
        expense: 0.0,
        id: null,
        income: 0.0,
        money: 0.0,
        name: '',
        transactions: null,
        isLoadFinished: false,
        activeSections: [2, 0],
        imageSet: null
    }
    onChange = (activeSections) => {
        this.setState({ activeSections });
    };
    UNSAFE_componentWillMount = () => {
        this.getDefaultData()
    }

    componentDidMount = () => {

    }
    getDefaultData = () => {
        const { wallet } = this.props
        this.setState({
            ...wallet,
            isLoadFinished: true
        })

    }

    goToAddTransaction = () => {
        const { goToAddTransaction } = this.props
        goToAddTransaction()
    }

    createImages = (each) => {
        const gallerySection = each.imageSet.map((gallery, i) => {
            return (

                <Image key={i}
                    source={{ uri: gallery.url }}
                    resizeMod='contain'
                    style={{ width: 100, height: 80, marginRight: '5%' }}></Image>
            );
        });
        return gallerySection
    }
    alltransactions = ({ item, index }) => {
        return (
            <View>
                <Card>
                    <Card.Header
                        title={
                            <View style={{ flexDirection: 'column' }}>
                                <SummaryText style={{ color: 'black' }}>{item.topic}</SummaryText>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: 'black', marginRight: '5%' }}>{item.type}</Text>
                                    <Text style={{ color: 'black' }}>{item.category}</Text>
                                </View>
                            </View>

                        }
                        extra={item.cash}
                    />
                    <Card.Body>
                        <View style={{ flexDirection: 'column' }}>

                            <Center style={{ flexDirection: 'row' }}>
                                {this.createImages(item)}
                            </Center>
                        </View>
                    </Card.Body>
                    <Card.Footer
                        content={
                            <Text>{item.date}</Text>
                        }
                    />
                </Card>
                <WhiteSpace />
            </View>


        )
    }
    render() {
        console.log('each yoyo', this.state.imageSet)
        return (
            <Container hideGoBack={false} title={"Wallet's Detail"}>
                <Center>
                    <Image style={{ width: 100, height: 100 }}
                        source={images.wallet2} resizeMod='contain'></Image>
                </Center>
                <HeaderText style={{ textAlign: 'center' }}>{this.state.name} : {this.state.money}
                </HeaderText>

                <View style={{
                    backgroundColor: 'white', paddingTop: '5%', height: 1000,
                    borderRadius: 20
                }} >
                    <Center style={{ paddingBottom: '2%' }}>
                        <TouchableOpacity onPress={this.goToAddTransaction}>
                            <Icon name="plus-circle" color="red" size="lg"></Icon>
                        </TouchableOpacity>
                    </Center>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <FlatList
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.transactions}
                            renderItem={this.alltransactions}
                        />
                    </ScrollView>
                </View>

            </Container>
        )
    }
}

export default WalletDetail
