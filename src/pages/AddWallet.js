import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Image, Alert, Text } from 'react-native';
import Container from '../components/general/Container';
import { ActivityIndicator, WhiteSpace, Card, Icon } from '@ant-design/react-native';
import {
    Center, SubmitButton, CenterContainer, MiniBoxXS, ImageButton,
    Logo, SummaryText, RowCenter, UserInput, AntFormContainer, ColumnCenter
} from '../components/General.styled'
import axios from 'axios';
import * as Progress from 'react-native-progress';
import { images } from "../utilities";
var _ = require('lodash');

class AddWallet extends Component {
    state = {
        id: null,
        name: '',
        money: 0,
        token: null
    }
    changeValue = (state, value) => this.setState({ [state]: value })

    checkValidation = () => {
        if (this.checkUserFillAllData()) {
            // check that both input passwords are the same
            const reg = /([1-9][0-9]*)|0/;
            if (this.state.money > 0 && reg.test(this.state.money) === true) {

                return true

            } else {
                this.alertError('☺ < please check again', '♦ Invalid input ')
                return false
            }

        } else {
            this.alertError('☺ < please fill in every form', '♦ Do not forget')
            return false
        }
    }

    alertError = (content, header) => {
        Alert.alert(header,
            content,
            [
                { text: 'Cancel' },
                { text: 'OK' },
            ],
            { cancelable: false })
    }
    checkUserFillAllData = () => {
        if (_.isEmpty(this.state.name && this.state.money)) {
            // user not fill all data
            return false
        } else {
            // user fill all data
            return true
        }
    }
    goToMainPage = () => {
        const { goToMainPage } = this.props
        goToMainPage()
    }
    createNewWallet = () => {
        if (this.checkValidation()) {
            axios.post('http://52.32.106.181:8888/wallets/add/1', {
                name: this.state.name,
                money: this.state.money
                // token: this.state.token
            }).then((response) => {
                console.log(response)
               this.goToMainPage()

            }).catch(function (error) {
                console.log('error: ', error)
            });
        }

    }


    render() {
        return (
            <Container hideGoBack={false}>
                <CenterContainer style={{ marginTop: '20%' }}>
                    <Logo source={images.wallet2} resizeMode={'contain'}></Logo>
                </CenterContainer>
                <View style={{
                    backgroundColor: 'white', marginTop: '30%', height: '50%',
                    borderRadius: 20, padding: '10%', marginBottom: '8%'
                }} >

                    <UserInput clear type="text" placeholder="Wallet name"
                        maxLength={20}
                        onChangeText={value => this.changeValue('name', value)}>
                        <Icon name="wallet" color="#0074D9" />
                    </UserInput>

                    <WhiteSpace />

                    <UserInput clear type="number" placeholder="money"
                        maxLength={6}
                        onChangeText={value => this.changeValue('money', value)}>
                        <Icon name="dollar" color="#0074D9" />
                    </UserInput>
                    <Center style={{ marginTop: '10%' }}>
                        <TouchableOpacity onPress={this.createNewWallet}>
                            <ImageButton source={images.add}></ImageButton>
                        </TouchableOpacity>
                    </Center>

                </View>
            </Container>
        )
    }

}
export default AddWallet
