import React, { Component } from 'react';
import axios from 'axios';
import { TouchableOpacity, ImageBackground, Image, View } from 'react-native'
import { Icon, WhiteSpace } from '@ant-design/react-native';
import {
    Body, FlextContent,
    CenterContainer, Logo, LinkText,
    ImageButton, UserInput, ColumnCenter, AntFormContainer
} from '../components/General.styled'
import { images } from "../utilities";

class LoginPage extends Component {
   
    UNSAFE_componentWillMount = () => {
      
    }
    goToMainPage = () => {
        const { goToMainPage } = this.props
        goToMainPage()
    }
    goToRegisterPage = () => {
        const { goToRegisterPage } = this.props
        goToRegisterPage()
    }


    render() {
        return (
            <Body >
                <ImageBackground style={{ flex: 1 }} source={images.loginBackground}>
                    <CenterContainer>
                        <Logo source={images.logo} resizeMode={'contain'}></Logo>
                    </CenterContainer>
                </ImageBackground>
                <FlextContent>
                    <AntFormContainer >
                        <UserInput clear type="text" placeholder="Username">
                            <Icon name="user" color="#0074D9" />
                        </UserInput>

                        <WhiteSpace />
                        <WhiteSpace />

                        <UserInput clear type="password" placeholder="Password" >
                            <Icon name="lock" color="#0074D9" />
                        </UserInput>

                    </AntFormContainer>

                    <WhiteSpace />
                    <WhiteSpace />

                    <ColumnCenter >

                        <TouchableOpacity onPress={this.goToMainPage}>
                            <ImageButton source={images.subBackground}></ImageButton>
                        </TouchableOpacity>

                        <WhiteSpace />
                        <WhiteSpace />

                        <TouchableOpacity  onPress={this.goToRegisterPage}>
                            <LinkText>Register</LinkText>
                        </TouchableOpacity>
                    </ColumnCenter>

                </FlextContent>
            </Body>
        )
    }
}
export default LoginPage