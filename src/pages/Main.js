import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Image, FlatList, Text } from 'react-native';
import Container from '../components/general/Container';
import { ActivityIndicator, WhiteSpace, Card, Icon } from '@ant-design/react-native';
import {
    Center, HeaderText, MiniBox, MiniBoxXS, CenterContainer,
    Logo, SummaryText, RowCenter
} from '../components/General.styled'
import axios from 'axios';
import * as Progress from 'react-native-progress';
import { images } from "../utilities";
var _ = require('lodash');

class MainPage extends Component {

    state = {
        isLoadFinished: false,
        user: {
            id: null,
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            bankBalance: 0.0,
            userImage: '',
        },
        wallets: [],
        totalExpense: 0.0,
        totalIncome: 0.0,
        isSelectToday: true
    }
    UNSAFE_componentWillMount = () => {
        this.getData()

    }
    getData = () => {
        let result = []
        let collection = []
        collection.push(axios.get('http://52.32.106.181:8888/wallets/userID/1'))
        collection.push(axios.get('http://52.32.106.181:8888/users/id/1'))

        Promise
            .all(collection).then(function (values) {
                values.forEach(element => {
                    console.log(element.data)
                    result.push(element.data)
                })
            }).then(() => {

                this.setState({ wallets: result[0] })
                this.setState({ user: result[1] })
                this.setState({ isLoadFinished: true })
            }).then(() => {

                this.getTotalIncome()
                this.getTotalExpense()
            })
    }

    getTotalIncome = () => {
        var output = 0.0
        if (this.state.wallets != []) {
            var items = this.state.wallets.map((item, key) =>
                item.income
            )
            output = items.reduce((prev, next) => prev + next, 0)
        }

        this.setState({ totalIncome: output })
    }
    getTotalExpense = () => {
        var output = 0.0
        if (_.isEmpty(this.state.wallets)) {
            output = 0.0
        } else {
            var items = this.state.wallets.map((item, key) =>
                item.expense
            )
            output = items.reduce((prev, next) => prev + next, 0) * -1
        }

        this.setState({ totalExpense: output })
    }


    calculateProgress = () => {
        var paid = 0.0
        if (_.isEmpty(this.state.wallets)) {
            paid = 0.0
        } else {
            var total = this.state.totalExpense
            const finalResult = (total / this.state.user.bankBalance)
            paid = finalResult
        }
        return paid
    }
    countAmountOfWallet = () => {

        var output = this.state.wallets.length
        return output
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    goToWalletDetail = (item, index) => {
        const { goToWalletDetail } = this.props
        let sentItem = {
            name: item.name,
            money: item.money,
            income: item.income,
            expense: item.expense,
            transactions: item.transactions,
            id: item.id,
        }
        goToWalletDetail(sentItem)

    }


    renderWallet = ({ item, index }) => {
        console.log('item', item)
        return (
            <TouchableOpacity onPress={() => this.goToWalletDetail(item, index)}>
                <Card >
                    <Card.Header
                        title={item.name}
                    />
                    <Card.Body>
                        <View >
                            <MiniBox style={{ height: 75 }}>
                                <Image source={images.wallet2} resizeMod='contain'
                                    style={{ width: 100, height: 80 }}></Image>

                                <HeaderText style={{
                                    color: 'black',
                                    textAlign: 'center',
                                    textAlignVertical: "center"
                                }}>: {item.money}</HeaderText>


                            </MiniBox>
                        </View>
                    </Card.Body>
                    <Card.Footer
                        content={
                            <Center>
                                <Icon name="arrow-up" color="green">{item.income}</Icon>
                            </Center>
                        }
                        extra={
                            <Center>
                                <Icon name="arrow-down" color="red">{item.expense}</Icon>
                            </Center>
                        }
                    />
                </Card>
            </TouchableOpacity>

        )
    }
    render() {
        return (
            <Container hideGoBack={true} title={"Money Go Where"}>
                {this.state.isLoadFinished ? (
                    <View style={{ backgroundColor: 'white', paddingTop: '5%' }} >
                        <Center  >
                            <HeaderText style={{ color: '#0074D9' }}>
                                {this.state.user.firstName}</HeaderText>
                            <SummaryText style={{ color: '#0e2f44' }}>
                                Bank balance {this.state.user.bankBalance}</SummaryText>
                        </Center>

                        <Progress.Bar progress={this.calculateProgress()}
                            width={300} height={20} color="#0074D9"
                            style={{ margin: '5%' }}
                            unfilledColor="#eeeeee" borderWidth={0} />

                        <MiniBox style={{
                            backgroundColor: '#eeeeee',
                            marginBottom: '5%'
                        }}>
                            <MiniBoxXS>
                                <SummaryText style={{ color: '#0074D9' }}>expense</SummaryText>
                                <SummaryText style={{ color: '#0e2f44' }}>
                                    {this.state.totalExpense}</SummaryText>
                            </MiniBoxXS>
                            <MiniBoxXS>
                                <SummaryText style={{ color: '#0074D9' }}>income </SummaryText>
                                <SummaryText style={{ color: '#0e2f44' }}>
                                    {this.state.totalIncome}</SummaryText>
                            </MiniBoxXS>

                        </MiniBox>

                        <SummaryText style={{ color: '#0e2f44' }}>
                            Your Wallets: {this.countAmountOfWallet()}</SummaryText>


                        <View style={{ height: 333, paddingTop: '5%' }} >
                            <ScrollView
                                showsVerticalScrollIndicator={false}
                            >
                                <FlatList
                                    keyExtractor={(item, index) => index.toString()}
                                    data={this.state.wallets}
                                    renderItem={this.renderWallet}
                                />
                            </ScrollView>
                        </View>


                    </View>

                ) : (
                        <View style={{ width: '100%', height: '100%', backgroundColor: 'white' }}>
                            <CenterContainer style={{ flex: 1 }}>
                                <Logo source={images.logo}
                                    style={{ borderRadius: 0 }}></Logo>
                                <ActivityIndicator text="Loading..." size="large" style={{ flex: 2 }} />
                            </CenterContainer >

                        </View>

                    )}
            </Container>

        )

    }
}
export default MainPage