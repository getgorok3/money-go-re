import React, { Component } from 'react';
import axios from 'axios';
import { TouchableOpacity, ImageBackground, Alert } from 'react-native'
import { Icon, WhiteSpace } from '@ant-design/react-native';
import {
    Body, FlextContent,
    CenterContainer, Logo, LinkText, ButtonText,
    ImageButton, UserInput, ColumnCenter, AntFormContainer
} from '../components/General.styled'
import { images } from "../utilities";

var _ = require('lodash');

class RegisterPage extends Component {

    state = {
        email: '',
        password: '',
        confirmPass: '',
        firstname: '',
        lastname: '',
        id: null,
        token: null
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    checkValidation = () => {
        if (this.checkUserFillAllData()) {
            // check that both input passwords are the same
            const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (this.state.password === this.state.confirmPass && reg.test(this.state.email) === true) {
                return true
            } else {
                this.alertError('☺ < please check again', '♦ Invalid input ')
                return false
            }

        } else {
            this.alertError('☺ < please fill in every form', '♦ Do not forget')
            return false
        }
    }
    goToMainPage = () => {
        const { goToMainPage } = this.props
        goToMainPage()
    }
    createNewUserAccount = () => {
        if (this.checkValidation()) {
            axios.post('http://52.32.106.181:8888/users/add/', {
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstname,
                lastName: this.state.lastname
            }).then((response) => {
                this.setState({
                    token: response.data.token,
                    firstname: response.data.firstName,
                    lastname: response.data.lastName,
                    id: response.data.id
                })
                this.props.onSuccess(this.state)
                console.log(response.data)
                this.goToMainPage()

            }).catch(function (error) {
                console.log('error: ', error.response.data.errors)
            });
        }

    }

    alertError = (content, header) => {
        Alert.alert(header,
            content,
            [
                { text: 'Cancel' },
                { text: 'OK' },
            ],
            { cancelable: false })
    }

    checkUserFillAllData = () => {
        if (_.isEmpty(this.state.email && this.state.password && this.state.confirmPass
            && this.state.firstname && this.state.lastname)) {
            // user not fill all data
            return false
        } else {
            // user fill all data
            return true
        }
    }
    render() {
        return (
            <Body >
                <ImageBackground style={{ height: 180, width: '100%' }} source={images.loginBackground}>
                    <CenterContainer>
                        <Logo source={images.welcomeText} resizeMode={'contain'}></Logo>
                    </CenterContainer>
                </ImageBackground>
                <FlextContent>
                    <AntFormContainer >
                        <UserInput clear type="text" placeholder="Username@ex.com"
                            onChangeText={value => this.changeValue('email', value)}>
                            <Icon name="user" color="#0074D9" />
                        </UserInput>

                        <WhiteSpace />

                        <UserInput clear type="password" placeholder="Password"
                            onChangeText={value => this.changeValue('password', value)}>
                            <Icon name="lock" color="#0074D9" />
                        </UserInput>

                        <WhiteSpace />

                        <UserInput clear type="password" placeholder="Confirm password"
                            onChangeText={value => this.changeValue('confirmPass', value)} >
                            <Icon name="lock" color="#0074D9" />
                        </UserInput>

                        <WhiteSpace />

                        <UserInput clear type="text" placeholder="First name"
                            onChangeText={value => this.changeValue('firstname', value)} >
                            <Icon name="idcard" color="#0074D9" />
                        </UserInput>

                        <WhiteSpace />

                        <UserInput clear type="text" placeholder="Last name"
                            onChangeText={value => this.changeValue('lastname', value)}>
                            <Icon name="idcard" color="#0074D9" />
                        </UserInput>

                    </AntFormContainer>

                    <WhiteSpace />
                    <WhiteSpace />

                    <ColumnCenter >

                        <TouchableOpacity onPress={this.createNewUserAccount}>
                            <ImageButton source={images.subBackground2}></ImageButton>
                        </TouchableOpacity>

                        <WhiteSpace />
                        <WhiteSpace />

                        <TouchableOpacity onPress={this.props.goToLoginPage}>
                            <LinkText>Back to Login</LinkText>
                        </TouchableOpacity>
                    </ColumnCenter>

                </FlextContent>
            </Body>
        )
    }

}
export default RegisterPage