
import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { store, history } from "./system/store";
import Router from './system/router'
import { ConnectedRouter } from 'connected-react-router'

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Router />
        </ConnectedRouter>
      </Provider>
    );
  }
}
