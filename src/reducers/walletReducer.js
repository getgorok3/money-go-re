import { WALLET_ADD } from "../constants/appConstants";

const DEFAULT_STATE = {
    name: '',
    money: 0.0,
    income: 0.0,
    expense: 0.0,
    transactions: null,
    id: null,
    
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case WALLET_ADD:
            return { ...state, ...payload }
        default:
            return state
    }
}
