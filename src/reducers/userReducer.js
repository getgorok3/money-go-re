import { USER_SAVE } from '../constants/appConstants'
const DEFAULT_STATE = {
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    imageUrl: '',
    id: null,
    bankBalance: 0.0,
    token: ''
}
export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case USER_SAVE:
            return { ...state, ...payload }
        default:
            return state
    }
}
