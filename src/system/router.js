import React, { Component } from 'react'
import { Route, Switch } from 'react-router-native'
import {
    ROUTE_TO_MAIN, ROUTE_TO_LOGIN, ROUTE_TO_REGISTER,
    ROUTE_TO_WALLETDETAIL, ROUTE_TO_ADDTRANSATION, ROUTE_TO_ADDWALLET
} from "../constants/routeConstants";
import {
    MainPage, LoginPage, RegisterPage,
    WalletDetailPage, AddTransactionPage,AddWalletPage
} from "../containers/index";

export default class Router extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={ROUTE_TO_LOGIN} component={LoginPage} />
                <Route exact path={ROUTE_TO_MAIN} component={MainPage} />
                <Route exact path={ROUTE_TO_REGISTER} component={RegisterPage} />
                <Route exact path={ROUTE_TO_WALLETDETAIL} component={WalletDetailPage} />
                <Route exact path={ROUTE_TO_ADDTRANSATION} component={AddTransactionPage} />
                <Route exact path={ROUTE_TO_ADDWALLET} component={AddWalletPage} />
            </Switch>
        )
    }
}