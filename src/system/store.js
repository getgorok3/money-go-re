import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import logger from 'redux-logger'
import Reducers from './reducers'

export const history = createMemoryHistory()
export const store = createStore(
    Reducers(history),
    compose(applyMiddleware(routerMiddleware(history), logger))
)