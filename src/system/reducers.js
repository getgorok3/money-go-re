import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import walletReducer from '../reducers/walletReducer'
import userReducer from '../reducers/userReducer'

const reducers = history =>
    combineReducers({
        wallet: walletReducer,
        user: userReducer,
        router: connectRouter(history)
    })

export default reducers