export  const images ={
    logo: require('./logo.png'),
    loginBackground: require('./loginBG2.jpg'),
    subBackground: require('./miniBG.png'),
    welcomeText: require('./welcome.png'),
    subBackground2: require('./miniBg2.png'),
    today: require('./today.png'),
    history: require('./history.png'),
    wallet: require('./wallet.png'),
    wallet2: require('./wallet2.png'),
    add: require('./add.png'),
}