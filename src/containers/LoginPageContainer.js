import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import LoginPage from "../pages/Login";
import {ROUTE_TO_MAIN,ROUTE_TO_REGISTER } from "../constants/routeConstants";
import { userSave } from "../actions/AppActions";

const mapStateToProps = ({ user }) => ({
    email: user.email,
    password: user.password,
    id: user.id,
    token: user.token
})

const mapDispatchToProps = dispatch => ({
    goToMainPage: () =>{
        dispatch(push(ROUTE_TO_MAIN))
    },
    onSuccess: user => {
        dispatch(userSave(user))
    },
    goToRegisterPage: () =>{
        dispatch(push(ROUTE_TO_REGISTER))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage)