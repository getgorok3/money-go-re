import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import RegisterPage from "../pages/Register";
import {ROUTE_TO_LOGIN,ROUTE_TO_MAIN } from "../constants/routeConstants";
import { userSave } from "../actions/AppActions";

const mapStateToProps = ({ user }) => ({
   user
})

const mapDispatchToProps = dispatch => ({
    goToLoginPage: () =>{
        dispatch(push(ROUTE_TO_LOGIN))
    },
    goToMainPage: () =>{
        dispatch(push(ROUTE_TO_MAIN))
    },
    onSuccess: user => {
        dispatch(userSave(user))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterPage)