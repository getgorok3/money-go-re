import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import WalletDetailPage from '../pages/WalletDetail'
import { ROUTE_TO_ADDTRANSATION, } from "../constants/routeConstants";

const mapStateToProps = ({ wallet }) => ({
    wallet
})
const mapDispatchToProps = dispatch => ({
    goToAddTransaction: () => {
        dispatch(push(ROUTE_TO_ADDTRANSATION))
    },

})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WalletDetailPage)