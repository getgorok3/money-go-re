import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import MainPage from "../pages/Main";
import { walletSave } from "../actions/AppActions"
import { ROUTE_TO_WALLETDETAIL } from "../constants/routeConstants";

const mapStateToProps = ({ wallet, user }) => ({
    wallet,
    user: user
})

const mapDispatchToProps = dispatch => ({
    goToWalletDetail: wallet => {
        dispatch(walletSave(wallet)),
            dispatch(push(ROUTE_TO_WALLETDETAIL))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPage)