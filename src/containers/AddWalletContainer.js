import { connect } from 'react-redux'
import { push,goBack } from 'connected-react-router'
import AddWallet from "../pages/AddWallet";
import {ROUTE_TO_MAIN,ROUTE_TO_REGISTER } from "../constants/routeConstants";
import { userSave } from "../actions/AppActions";

const mapStateToProps = ({ user }) => ({
   user
})

const mapDispatchToProps = dispatch => ({
    goToMainPage: () =>{
        dispatch(push(ROUTE_TO_MAIN))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddWallet)