

export { default as MainPage } from './MainPageContainer'
export { default as LoginPage } from './LoginPageContainer'
export { default as RegisterPage } from './RegisterContainer'
export { default as WalletDetailPage } from './walletDetailContainer'
export { default as AddTransactionPage } from './AddTransactionContainer'
export { default as AddWalletPage } from './AddWalletContainer'