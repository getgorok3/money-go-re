import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import AddTransaction from "../pages/AddTransaction";
import {ROUTE_TO_MAIN,ROUTE_TO_REGISTER } from "../constants/routeConstants";
import { userSave } from "../actions/AppActions";

const mapStateToProps = ({ user }) => ({
   user
})

const mapDispatchToProps = dispatch => ({
   
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddTransaction)